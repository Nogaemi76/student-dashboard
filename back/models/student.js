
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var studentSchema = new Schema({
        name: String,
        studentId: Number,
        email: String,
        password: String,
        class: String,
        message: [{
                body: String,
                dateTime: Date
        }],
        projet: [{
                note: Number,
                body: String,
                dateTime: Date
        }],
        absence: [{
                dateTime: Date
        }],
        late: [{
                dateTime: Date
        }]

});

module.exports = mongoose.model('student', studentSchema, 'students_dev')