const mongoose = require ('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema({
        name: String,
        email: String,
        password: String,
        class: String,
        status: String
})

module.exports = mongoose.model('user', userSchema, 'user_promo')