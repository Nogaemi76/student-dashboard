const express = require('express')
const router = express.Router()
//bring here student.js that was created inside the models folder this variable should be start with capital letter
const Student = require('../models/student')

//for connect db
const mongoose = require('mongoose')
let db = 'mongodb://localhost:27017/promo_dev'
mongoose.connect(db, { useNewUrlParser: true }, err => {
        if (err) {
                console.log('Error!! ' + err)
        } else {
                console.log('Connected mongodb')
        }
})

router.get('/', (req, res) => {
        res.send('From Api route HI HI Emille!!!')
})

router.post('/registerStu', (req, res) => {
        //extract the student information from the body 
        let studentData = req.body
        //next this studentData has to be cast into student models to understand the mongoDB
        let student = new Student(studentData)
        //above the student model or student objet simply call to save mode in DB
        student.save((error, registerStudent) => {
                if (error) {
                        console.log(error)
                } else {
                        res.status(200).send(registerStudent)
                }
        })
})

router.get('/listStu', (req, res) => {
        Student.find((error, listStudent) => {
                if (error) {
                        console.log(error)
                } else {
                        console.log(listStudent)
                        res.status(200).send(listStudent)
                }
        })
})


module.exports = router