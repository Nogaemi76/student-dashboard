const express = require('express')
const router = express.Router()
//bring here student.js that was created inside the models folder this variable should be start with capital letter
const User = require ('../models/user')

//for connect db
const mongoose = require('mongoose')
let db = 'mongodb://localhost:27017/promo_dev'
mongoose.connect(db, { useNewUrlParser: true }, err => {
        if (err) {
                console.log('Error!! ' + err)
        } else {
                console.log('Connected mongodb')
        }
})

router.get('/', (req, res) =>{
        res.send('From Api route HI HI Emille!!!')
})

router.post('/registerStu', (req, res) => {
        //extract the student information from the body 
        let studentData = req.body
        //next this studentData has to be cast into student models to understand the mongoDB
        let student = new User(studentData)
        //above the student model or student objet simply call to save mode in DB
        student.save((error, registerStudent) => {
                if(error) {
                        console.log(error)
                }else{
                        res.send(registerStudent)
                }
        })
})

router.post('/loginStu', (req, res) =>{
        let studentData = req.body
        User.findOne({email: studentData.email}, (error, student) =>{
                if (error){
                        //pourquoi error ici directement !student
                        console.log(error)
                }else {
                        console.log(student)
                        if(!student) {
                                res.send('Invalide email')   
                        }
                        else if(student.password !== studentData.password) {
                                res.send('Invalide password')   
                        }else {
                                res.send(student)
                        }
                    
                }
        })

})



////Prof inscription 

router.post('/registerProf', (req, res) => {
        //extract the student information from the body 
        let profData = req.body
        let prof = new User(profData)
         prof.save((error, registerProfes) => {
                if (error) {
                        console.log(error)
                } else {
                        res.status(200).send(registerProfes)
                }
        })
})

router.post('/loginProf', (req, res) => {
        //extract the student information from the body 
        let profData = req.body
        //check the email & pw are exist  in the DB :::use the Student mongo model
        User.findOne({ email: profData.email }, (error, profes) => {
                if (error) {
                        
                        console.log(error)
                } else {
                        if (!profes) {
                                res.send('Invalide email')
                        }
                        else if (profes.password !== profData.password) {
                                res.send('Invalide password')
                        } else {
                                res.send(profes)
                        }

                }
        })

})

module.exports = router