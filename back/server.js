const express = require ('express')
const bodyParser = require('body-parser')

const PORT = 4042;
//require or import router api from the folder routes 
const api = require ('./routes/apiStu')
const app = express()

app.use(bodyParser.json())
//use router start with index name api and the variable api


app.use('/api', api)
//first argument callback function for test
app.get('/', (req, res) => {
        res.send("Hi Emille how are you??????")
})


app.listen(PORT, () => {
        console.log('server is running in the PORT: ' + PORT)
})
