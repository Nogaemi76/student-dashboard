import { NgModule } from '@angular/core';
import { 
  MatFormFieldModule, 
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatRadioModule
} from '@angular/material'; 

const MaterialComponents = [
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatRadioModule
]

@NgModule({
  imports: [MaterialComponents],
  exports: [MaterialComponents]
})
export class MaterialModule { }
